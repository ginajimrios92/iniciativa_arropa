#
# Author: Gina Jiménez
# Maintainer(s): Oscar Elton, Mariana Solano, Gi Jimenez, Adrián Lara, Alicia Franco
# License: (c) Data Cívica 2020, GPL v2 or newer
#
# ------------------------------------------------------------------------------------
# Iniciativa-arropa/import-clean/import-sinais.r
#

require(pacman)
p_load(argparse, foreign, tidyverse, janitor, here, googledrive, stringi, stringr,
       data.table)

bases_inp <- "1_QkndWRFjdAYNDNE4h_-eh-eLMA-45YI"
files <- list(nommun = here("import-clean/out/nom-mun.rds"),
              causas = here("import-clean/out/cat-causas.rds"),
              causas90a97 = here("import-clean/out/DETALLADA.dbf"),
              parent = here("import-clean/out/cat-parent.rds"),
              output = here("clean-data/out/sinais-all.rds"))


# ====================== Catalogues ====================== #
parent <- readRDS(files$parent)
causas <- readRDS(files$causas)
nommun <- readRDS(files$nommun)
causas90a97 <- read.dbf(files$causas90a97)

# ===================== Global funtions =================== #
clean_text <- function(s){
  str_squish(toupper(stri_trans_general(s, "latin-ascii")))
}

# ============= Patterns for place and cause ============== #
patP_viv <- "EN VIVIENDA|INSTITUCION RESIDENCIAL"
patP_via <- "CALLES Y CARRETERAS|AREA INDUSTRIAL Y DE LA CONSTRUCCION|AREAS DE DEPORTE Y ATLETISMO|COMERCIO Y AREA DE SERVICIOS|EN ESCUELAS"
patP_otr <- "OTRO LUGAR ESPECIFICADO|GRANJA"
patP_ne <- "LUGAR NO ESPECIFICADO"
patC_af <- "ARMA DE FUEGO|ARMAS DE FUEGO|ARMA LARGA|ARMA CORTA"
patC_ab <- "OBJETO CORTANTE|OBJETO ROMO O SIN FILO|CUCHILLO"
patC_ahr <- "AHORCAMIENTO|AHOGAMIENTO"
patC_env <- "DROGAS, MEDICAMENTOS Y SUSTANCIAS BIOLOGICAS|PRODUCTOS QUIMICOS Y SUSTANCIAS NOCIVAS|PLAGUICIDAS|SUSTANCIA CORROSIVA"
patC_fc <- "FUERZA CORPORAL|GOLPE CONTRA|APORREO"
patC_ne <- "MEDIOS NO ESPECIFICADOS"

pat_resp98_1 <- "J"
pat_resp98_2 <- "U0|A1|C3"
pat_resp98_3 <- "B90|I26|I27|I28|A37|D14|D19|B38|B39" 
pat_resp98_4 <- "C450|B206|E662|C761|G473|B440|B441|E840"

#Maybe de lo deemás
pat_resp90_1 <- "TUBERCULOSIS|TUBERCULOSA|"
pat_resp90_2 <- "U0|A1|C3"
pat_resp90_3 <- "B90|I26|I27|I28|A37|D14|D19|B38|B39" 
pat_resp90_4 <- "C450|B206|E662|C761|G473|B440|B441|E840"



#### Función para importar SINAIS ####
names <- c("90","91","92","93","94","95","96","97","98", "99",
           "00", "01", "02", "03", "04", "05", "06",
            "07", "08", "09", "10", "11", "12",
           "13", "14", "15", "16", "17", "18", "19", "20")

drive <- drive_ls(as_id(bases_inp))
tab <- paste0(paste0("DEFUN", names, ".csv")) 
tmp <- paste(tempdir(), tab, sep = "/")
walk2(drive$id[order(match(drive$name, tab))],
      tmp,
      ~ drive_download(as_id(.x), path = .y, overwrite = TRUE))



#### Importar y pegar ####
sinais1 <- data.frame()
for (i in 9:length(names)){
     defun <-  read.csv(tmp[i])%>% 
               clean_names() %>% 
               mutate(ent_ocurr = formatC(as.integer(ent_ocurr), width = 2, flag = 0, format = "d"),
                      mun_ocurr = formatC(as.integer(mun_ocurr), width = 3, flag = 0, format = "d"),
                      inegi = paste0(ent_ocurr, mun_ocurr))%>%
                select(one_of(c("inegi", "causa_def", "sexo", "edad", 
                                "anio_regis", "dia_ocurr", "mes_ocurr", 
                                "anio_ocur", "escolarida", "edo_civil", 
                                "vio_fami", "par_agre", "presunto")))
       sinais1 <- bind_rows(sinais1, defun)
               
  print(paste0("Ya escribí la base del ", names[i]))
}

sinais1 <- left_join(sinais1, causas)


sinais2 <- data.frame()
for (i in 1:8){
  defun <-  read.csv(tmp[i])%>% 
            clean_names() %>% 
            mutate(ent_ocurr = formatC(as.integer(ent_ocurr), width = 2, flag = 0, format = "d"),
                   mun_ocurr = formatC(as.integer(mun_ocurr), width = 3, flag = 0, format = "d"),
                   inegi = paste0(ent_ocurr, mun_ocurr),
                   anio_ocur=as.numeric(anio_ocur),
                   anio_ocur=case_when(anio_ocur==90 ~ 1990,
                                       anio_ocur==91 ~ 1991,
                                       anio_ocur==92 ~ 1992,
                                       anio_ocur==93 ~ 1993,
                                       anio_ocur==94 ~ 1994,
                                       anio_ocur==95 ~ 1995,
                                       anio_ocur==96 ~ 1996,
                                       anio_ocur==97 ~ 1997,
                                       anio_ocur==99 ~ 9999))%>%
              select(one_of(c("inegi", "causa_def", "sexo", "edad", 
                    "anio_regis", "dia_ocurr", "mes_ocurr", 
                    "anio_ocur", "escolarida", "edo_civil", 
                    "vio_fami", "par_agre", "presunto")))
  sinais2 <- bind_rows(sinais2, defun)
  
  print(paste0("Ya escribí la base del ", names[i]))
}

sinais2 <- left_join(sinais2, causas90a97,
                     by=c("causa_def"="CLAVE"))%>%
           rename(causa=DESCRIP)

sinais_all <- bind_rows(sinais1, sinais2)

saveRDS(sinais_all, here("import-clean/out/sinais-todas.rds"))

#### Limpiar ####
sinais_all <- sinais1 %>% 
              rename(year=anio_ocur) %>% 
              filter(year>=1998 & year!=9999) %>% 
              mutate(dia=formatC(dia_ocurr, width = 2, flag = 0, format = "d"),
                     mes=formatC(mes_ocurr, width = 2, flag = 0, format = "d"),
                     fecha=paste0(year, "/", mes, "/", dia),
                     fecha=as.Date(fecha,  "%Y/%m/%d"),
                     mes=as.integer(mes),
                     mes =factor(mes, levels=c(1:12), labels=c("Enero", "Febrero",
                                                               "Marzo", "Abril", 
                                                               "Mayo", "Junio", "Julio",
                                                               "Agosto", "Septiembre",
                                                               "Octubre", "Noviembre", 
                                                               "Diciembre")),
         sexo = case_when(sexo==1 ~ "Hombre",
                          sexo==2 ~ "Mujer",
                          T ~ NA_character_),
         edad = as.integer(edad),
         edad = case_when(substr(edad, 1, 2) %in% 40:41 ~ as.integer(substr(edad, 2, 4)),
                          substr(edad, 1, 2) < 40 ~ as.integer(0),
                          T ~ NA_integer_),
         escolarida = as.integer(escolarida),
         escolarida = case_when(anio_regis < 2012 & escolarida == 1 ~  "Sin escolaridad",
                                anio_regis < 2012 & escolarida %in% 2:3 ~ "Preescolar",
                                anio_regis < 2012 & escolarida == 4 ~ "Primaria",
                                anio_regis < 2012 & escolarida == 5 ~ "Secundaria",
                                anio_regis < 2012 & escolarida == 6 ~ "Preparatoria", 
                                anio_regis < 2012 & escolarida == 7 ~ "Licenciatura o más",
                                anio_regis < 2012 & escolarida == 8 ~ "No aplica",
                                anio_regis < 2012 & escolarida == 9 ~ "No especificado",
                                anio_regis >= 2012 & escolarida == 1 ~ "Sin escolaridad",
                                anio_regis >= 2012 & escolarida %in% 2:3 ~ "Preescolar",
                                anio_regis >= 2012 & escolarida %in% 4:5 ~ "Primaria",
                                anio_regis >= 2012 & escolarida %in% 6:7 ~ "Secundaria",
                                anio_regis >= 2012 & escolarida == 8 ~ "Preparatoria",
                                anio_regis >= 2012 & escolarida %in% 9:10 ~ "Licenciatura o más", 
                                anio_regis >= 2012 & escolarida == 88 ~ "No aplica",
                                anio_regis >= 2012 & escolarida == 99 ~ "No especificado"),
         escolarida = factor(escolarida, levels = c("No especificado", "Sin escolaridad", "Preescolar",
                                                    "Primaria", "Secundaria", "Preparatoria", "Licenciatura o más")),
         edo_civil = case_when(anio_regis <= 2003 & edo_civil==1 ~ "Soltero",
                               anio_regis <= 2003 & edo_civil==2 ~ "Casado",
                               anio_regis <= 2003 & edo_civil==3 ~ "Unión Libre",
                               anio_regis <= 2003 & edo_civil %in% 4:5 ~ "Separado o Divorciado",
                               anio_regis <= 2003 & edo_civil==6 ~ "Viudo",
                               anio_regis <= 2003 & edo_civil==9 ~ "No especificado",
                               anio_regis %in% 2004:2011 & edo_civil==1 ~ "Soltero",
                               anio_regis %in% 2004:2011 & edo_civil==2 ~ "Viudo",
                               anio_regis %in% 2004:2011 & edo_civil==3 ~ "Separado o Divorciado",
                               anio_regis %in% 2004:2011 & edo_civil==4 ~ "Unión Libre",
                               anio_regis %in% 2004:2011 & edo_civil==5 ~ "Casado",
                               anio_regis %in% 2004:2011 & edo_civil==8 ~ "No aplica",
                               anio_regis %in% 2004:2011 & edo_civil==9 ~ "No especificado",
                               anio_regis >= 2012 & edo_civil==1 ~ "Soltero",
                               anio_regis >= 2012 & edo_civil %in% c(2,6) ~ "Separado o Divorciado",
                               anio_regis >= 2012 & edo_civil==3 ~ "Viudo",
                               anio_regis >= 2012 & edo_civil==4 ~ "Unión Libre",
                               anio_regis >= 2012 & edo_civil==5 ~ "Casado",
                               anio_regis >= 2012 & edo_civil==8 ~ "No aplica",
                               anio_regis >= 2012 & edo_civil==9 ~ "No especificado"),
         vio_fami = case_when(vio_fami==1 ~ "Hubo violencia familiar",
                              vio_fami==2 ~ "No hubo violencia familiar",
                              T ~ NA_character_),
         causa1=substr(causa_def, 0, 1),
         causa2=substr(causa_def, 0, 2),
         causa3=substr(causa_def, 0, 3),
         causa4=substr(causa_def, 0, 4)) %>% 
  left_join(parent) %>% 
  select(-par_agre) %>% 
  mutate(causa = clean_text(causa),
         respiratoria = case_when(causa1=="J" ~ "Muerte respiratoria",
                                  causa2 %like% pat_resp98_2 ~ "Muerte respiratoria",
                                  causa3 %like% pat_resp98_3 ~ "Muerte respiratoria",
                                  causa4 %like% pat_resp98_4 ~ "Muerte respiratoria",
                                  T ~ "Otro"),
         lugar = case_when(causa %like% patP_viv ~ "Vivienda",
                           causa %like% patP_via ~ "Vía pública",
                           causa %like% patP_otr ~ "Otro",
                           causa %like% patP_ne ~ "No especificado",
                           T ~ NA_character_),
         causa_hom = NA_character_,
         causa_hom = case_when(causa %like% patC_af ~ "Arma de fuego",
                               causa %like% patC_ab ~ "Arma blanca",
                               causa %like% patC_ahr ~ "Ahorcamiento o ahogamiento",
                               causa %like% patC_env ~ "Envenenamiento o sustancia corrosiva",
                               causa %like% patC_fc ~ "Fuerza corporal",
                               causa %like% patC_ne ~ "No especificado",
                               T ~ "Otro"))%>% 
  select(-causa) %>% 
  left_join(nommun) %>% 
  select(inegi, cve_ent, nom_ent, cve_mun, nom_mun, mes_ocurr, mes, fecha, year, sexo, edad, 
         escolarida, edo_civil, vio_fami, parentesco, lugar, causa_hom, respiratoria, presunto)

saveRDS(sinais_all, here("import-clean/out/sinais-clean.rds"))
# done.

data <- group_by(sinais_all, year, respiratoria, inegi)%>%
        summarize(total=n())%>%
        ungroup()

data <- filter(data, respiratoria=="Muerte respiratoria")

write.csv(data, "muertes-respiratorias.csv")

